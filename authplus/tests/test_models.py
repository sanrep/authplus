import pytest

from .factories import PermissionsPackageFactory, UserGroupFactory

# from authplus.models import User
# from ..models import PermissionsPackage, UserGroup

pytestmark = pytest.mark.django_db


# def test_user_get_absolute_url(user: User):
#     assert user.get_absolute_url() == f'/users/{user.username}/'

class TestPermissionsPackage:
    def test_string_representation(self):
        permissions_package = PermissionsPackageFactory()
        assert permissions_package.__str__() == permissions_package.name
        assert str(permissions_package) == permissions_package.name


class TestUserGroup:
    def test_usergroup___str__(self):
        user_group = UserGroupFactory()
        assert user_group.__str__() == user_group.name
        assert str(user_group) == user_group.name


class TestUser:
    def test_update_permissions(self, data):
        # users, user_groups, permissions, permissions_packages = data
        users = data['users']
        user_groups = data['user_groups']
        permissions = data['permissions']
        permissions_packages = data['permissions_packages']

        assert users[0].user_permissions.count() == 0
        # user0 (permission0)
        users[0].individual_permissions.add(permissions[0])
        users[0].update_permissions()
        assert users[0].user_permissions.count() == 1
        # user0 (permission0, permissions_package0)
        users[0].permissions_packages.add(permissions_packages[0])
        users[0].update_permissions()
        assert users[0].user_permissions.count() == 1
        # permissions_package0 (permission1)
        permissions_packages[0].permissions.add(permissions[1])
        permissions_packages[0].update_users()
        assert users[0].user_permissions.count() == 2
        # permissions_package1 (permission1, permission2)
        permissions_packages[1].permissions.add(permissions[1], permissions[2])
        permissions_packages[1].update_users()
        # user_groups0 (permissions_package1)
        user_groups[0].permissions_packages.add(permissions_packages[1])
        user_groups[0].update_users()
        # user_groups0 (permission3, permissions_package1)
        user_groups[0].permissions.add(permissions[3])
        # user_groups0 (permission3, permissions_package1, users_groups1)
        user_groups[0].subgroups.add(user_groups[1])
        user_groups[0].update_users()
        # user0 (permission0, permissions_package0, users_groups0)
        users[0].user_groups.add(user_groups[0])
        users[0].update_permissions()
        assert users[0].user_permissions.count() == 4
        # user1 (users_groups0)
        users[1].user_groups.add(user_groups[0])
        users[1].update_permissions()
        assert users[1].user_permissions.count() == 3
        # user2 (users_groups1)
        users[2].user_groups.add(user_groups[1])
        users[2].update_permissions()
        assert users[2].user_permissions.count() == 0
        # permissions_package3 (permission4, permission5)
        permissions_packages[3].permissions.add(permissions[4], permissions[5])
        permissions_packages[3].update_users()
        # user_groups1 (permissions_package3)
        user_groups[1].permissions_packages.add(permissions_packages[3])
        user_groups[1].update_users()
        assert users[0].user_permissions.count() == 6
        assert users[1].user_permissions.count() == 5
        assert users[2].user_permissions.count() == 2
        # user_groups0 (permission3, permissions_package1, users_groups1,
        # users_groups2)
        user_groups[0].subgroups.add(user_groups[2])
        user_groups[0].update_users()
        # user_groups1 (permissions_package3, user_groups2)
        user_groups[1].subgroups.add(user_groups[2])
        user_groups[1].update_users()
        # user2 (users_groups1, users_groups2)
        users[2].user_groups.add(user_groups[2])
        users[2].update_permissions()
        # permissions_package2 (permission3)
        permissions_packages[2].permissions.add(permissions[3])
        permissions_packages[2].update_users()
        # user_groups2 (permissions_package2)
        user_groups[2].permissions_packages.add(permissions_packages[2])
        user_groups[2].update_users()
        assert users[0].user_permissions.count() == 6
        assert len(user_groups[0]._get_subgroups()) == 2
        assert users[2].user_permissions.count() == 3

        # permissions_package1 (permission2)
        permissions_packages[1].permissions.remove(permissions[1])
        permissions_packages[1].update_users()
        assert users[0].user_permissions.count() == 6
        # permissions_package1 ()
        permissions_packages[1].permissions.remove(permissions[2])
        permissions_packages[1].update_users()
        assert users[0].user_permissions.count() == 5
        # user0 (permission0, permissions_package0)
        users[0].user_groups.remove(user_groups[0])
        users[0].update_permissions()
        assert users[0].user_permissions.count() == 2
        # user2 ()
        users[2].user_groups.clear()
        users[2].update_permissions()
        assert users[2].user_permissions.count() == 0
