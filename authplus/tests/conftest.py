import pytest

from .factories import (
    PermissionFactory,
    PermissionsPackageFactory,
    UserFactory,
    UserGroupFactory,
)


@pytest.fixture
def data():
    users = [UserFactory() for i in range(3)]
    user_groups = [UserGroupFactory() for i in range(3)]
    permissions = [PermissionFactory() for i in range(6)]
    permissions_packages = [PermissionsPackageFactory() for i in range(4)]
    data = {
        'users': users,
        'user_groups': user_groups,
        'permissions': permissions,
        'permissions_packages': permissions_packages,
    }
    return data
