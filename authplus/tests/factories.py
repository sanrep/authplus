from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

import factory
import factory.fuzzy

from ..models import PermissionsPackage, User, UserGroup


class ContentTypeFactory(factory.django.DjangoModelFactory):
    app_label = factory.fuzzy.FuzzyText()
    model = factory.fuzzy.FuzzyText()

    class Meta:
        model = ContentType


class PermissionFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    codename = factory.fuzzy.FuzzyText()
    content_type = factory.SubFactory(ContentTypeFactory)

    class Meta:
        model = Permission


class PermissionsPackageFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    description = factory.Faker(
        'paragraph',
        nb_sentences=3,
        variable_nb_sentences=True,
        locale='hr_HR',
    )

    class Meta:
        model = PermissionsPackage


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.fuzzy.FuzzyText()
    password = factory.fuzzy.FuzzyText()
    first_name = factory.fuzzy.FuzzyText()
    last_name = factory.fuzzy.FuzzyText()
    email = factory.Faker('safe_email')
    is_active = True
    is_staff = True
    is_superuser = False

    class Meta:
        model = User


class UserGroupFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    description = factory.Faker(
        'paragraph',
        nb_sentences=3,
        variable_nb_sentences=True,
        locale='hr_HR',
    )

    class Meta:
        model = UserGroup
