from django.urls import resolve, reverse


def test_login():
    assert reverse('login') == '/auth/login/'
    assert resolve('/auth/login/').view_name == 'login'


def test_logout():
    assert reverse('logout') == '/auth/logout/'
    assert resolve('/auth/logout/').view_name == 'logout'


def test_password_change():
    assert reverse('password_change') == '/auth/password_change/'
    assert resolve('/auth/password_change/').view_name == 'password_change'


def test_password_change_done():
    assert reverse('password_change_done') == '/auth/password_change_done/'
    assert resolve('/auth/password_change_done/').view_name == 'password_change_done'
