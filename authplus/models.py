from django.contrib.auth.models import AbstractUser, Permission
from django.db import models
from django.utils.translation import gettext_lazy as _


class AbstractPermissionsPackage(models.Model):
    """Permissions package is a generic way to categorize permissions.

    Permissions packages are used for easier applying a bunch of
    related permissions to a user. This can also be achieved using
    auth.models.Group, but it seems more natural to divide grouping of
    permissions to user and permission groups.

    Besides that, permissions related to package that are granted to a
    user are put into permissions field of a user on granting a package,
    rather than using recursive method when trying to determine whether
    user has particular permission.

    This is an abstract class to allow extension.
    """
    name = models.CharField(
        verbose_name=_('name'),
        max_length=255,
        unique=True,
    )
    description = models.TextField(
        verbose_name=_('description'),
        blank=True,
    )
    permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('permissions'),
        blank=True,
        help_text=_('Permissions included in the package.'),
    )

    class Meta:
        abstract = True
        verbose_name = _('permissions package')
        verbose_name_plural = _('permissions packages')

    def __str__(self):
        return self.name

    def get_users(self):
        all_users = set(self.user_set.all())
        groups = self.usergroup_set.all()
        all_groups = set(groups)
        for group in groups:
            all_groups.update(group._get_supergroups())
        for group in all_groups:
            all_users.update(group.user_set.all())
        return all_users

    def update_users(self):
        for user in self.get_users():
            user.update_permissions()


class PermissionsPackage(AbstractPermissionsPackage):
    """Permission package."""
    pass


class AbstractUserGroup(models.Model):
    """User group is a generic way to categorize users.

    User groups are used for easier categorizing users. This can also be
    achieved using auth.models.Group, but it seems more natural to
    divide grouping of users and user groups.

    User groups have a hierarchical structure. Each group can have
    several subgroups, and all the permissions granted to all subgroups.

    Permissions for user groups can be granted using individual
    permissions or permission packages.

    Besides that, permissions related to user as a member of different
    user groups are determined (put into permissions field of a user)
    on defining user group membership, rather than using recursive
    method when trying to determine whether user has particular
    permission.

    This is an abstract class to allow extension.
    """
    name = models.CharField(
        verbose_name=_('name'),
        max_length=255,
        unique=True,
    )
    description = models.TextField(
        verbose_name=_('description'),
        blank=True,
    )
    subgroups = models.ManyToManyField(
        'self',
        verbose_name=_('subgroups'),
        blank=True,
        symmetrical=False,
    )
    permissions_packages = models.ManyToManyField(
        PermissionsPackage,
        verbose_name=_('permissions packages'),
        blank=True,
    )
    permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('individual permissions'),
        blank=True,
    )

    class Meta:
        abstract = True
        verbose_name = _('user group')
        verbose_name_plural = _('user groups')

    def __str__(self):
        return self.name

    def _get_subgroups(self, catalog=None):
        if catalog is None:
            catalog = set()
        for subgroup in self.subgroups.all():
            if subgroup not in catalog:
                catalog.add(subgroup)
                subgroup._get_subgroups(catalog)
        return catalog

    def _get_supergroups(self, catalog=None):
        if catalog is None:
            catalog = set()
        for supergroup in UserGroup.objects.filter(subgroups=self):
            if supergroup not in catalog:
                catalog.add(supergroup)
                supergroup._get_supergroups(catalog)
        return catalog

    def get_users(self):
        all_users = set(self.user_set.all())
        all_groups = self._get_supergroups()
        for group in all_groups:
            all_users.update(group.user_set.all())
        return all_users

    def update_users(self):
        for user in self.get_users():
            user.update_permissions()


class UserGroup(AbstractUserGroup):
    """User group."""
    pass


class AbstractUserPlus(AbstractUser):
    """Project user.

    Inherited fields from auth.models.AbstractUser are:

    * date_joined (auth.models.AbstractUser)
    * email (auth.models.AbstractUser)
    * first_name (auth.models.AbstractUser)
    * groups (auth.models.PermissionsMixin)
    * is_active (auth.models.AbstractUser)
    * is_staff (auth.models.AbstractUser)
    * is_superuser (auth.models.PermissionsMixin)
    * last_login (auth.base_user.AbstractBaseUser)
    * last_name (auth.models.AbstractUser)
    * password (auth.base_user.AbstractBaseUser)
    * user_permissions (auth.models.PermissionsMixin)
    * username (auth.models.AbstractUser)

    This is an abstract class to allow extension.
    """

    user_groups = models.ManyToManyField(
        UserGroup,
        verbose_name=_('user groups'),
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions '
            'granted to each of his groups.'
        ),
    )
    permissions_packages = models.ManyToManyField(
        PermissionsPackage,
        verbose_name=_('permissions packages'),
        blank=True,
        help_text=_(
            'Permissions packages this user is granted. A user will get all '
            'permissions that are in the permissions package.'
        ),
    )
    individual_permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('individual permissions'),
        blank=True,
        help_text=_('Additional individual permissions this user is granted.'),
        related_name='individual_user_permissions'
    )

    class Meta:
        abstract = True
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_usergroups(self):
        all_groups = set()
        user_groups = self.user_groups.all()
        for group in user_groups:
            all_groups.update(group._get_subgroups(all_groups))
        all_groups.update(user_groups)
        return all_groups

    def get_permissions(self):
        all_permissions = set(self.individual_permissions.all())
        all_packages = set(self.permissions_packages.all())
        all_groups = set(self.get_usergroups())
        for group in all_groups:
            all_packages.update(group.permissions_packages.all())
            all_permissions.update(group.permissions.all())
        for package in all_packages:
            all_permissions.update(package.permissions.all())
        return all_permissions

    def update_permissions(self):
        self.user_permissions.set(self.get_permissions())


class User(AbstractUserPlus):
    """Project user."""
    pass
