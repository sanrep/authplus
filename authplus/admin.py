from django.contrib import admin
from django.contrib.auth.models import Permission

from .models import PermissionsPackage, User, UserGroup


class PermissionAdmin(admin.ModelAdmin):
    pass


class PermissionsPackageAdmin(admin.ModelAdmin):
    pass


class UserAdmin(admin.ModelAdmin):
    pass


class UserGroupAdmin(admin.ModelAdmin):
    pass


admin.site.register(Permission, PermissionAdmin)
admin.site.register(PermissionsPackage, PermissionsPackageAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(UserGroup, UserGroupAdmin)
